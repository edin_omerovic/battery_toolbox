'''
Discharges battery using predefined current pulse

Records the discharge pulse (time before the pulse and time after the pulse) using Joulescope.
'''
import csv
import datetime
from time import sleep
from loguru import logger

from variable_load.variable_load import VariableLoad
from jls_support.joulescope_interface import Joulescope

pulse_discharge_config = {
    "RELAX_TIME" : 35,
    "CUTOFF_VOLTAGE" : 2.1,
    "output_filepath" : "measurements/test.csv",
    "pulse_intensity" : -100e-6,
    "pulse_duration" : 30
}

variable_load_config = {
    "device_adress" : "GPIB0::24::INSTR"
}

joulescope_config = {
    "output_folder" : "measurements/",
    "initial_wait" : 3,
    "final_wait" : 5,
    "device_parameters" : {
        "sampling_frequency" : 200
    }
}

if __name__ == "__main__":
    logger.info("Opened pulse_discharge.py script")

    load = VariableLoad(variable_load_config, compliance_voltage = 3.1)
    joulescope = Joulescope(joulescope_config)

    RELAXATION_PERIOD = pulse_discharge_config["RELAX_TIME"]
    CUTOFF_VOLTAGE = pulse_discharge_config["CUTOFF_VOLTAGE"]
    output_filepath = pulse_discharge_config["output_filepath"]
    pulse_intensity = pulse_discharge_config["pulse_intensity"]
    pulse_duration = pulse_discharge_config["pulse_duration"]

    jls_output_folder = joulescope_config["output_folder"]

    count = 0
    logger.info("Started discharge procedure")

    try:
        while True:
            #Pokreni joulescope skriptu za snimanje
            jls_name = str(count) + ".jls"
            pulse_start_time = datetime.datetime.now()
            joulescope.start_capture(jls_output_folder + jls_name)

            sleep(joulescope_config["initial_wait"])

            with load: #Generate a discharge pulse
                load.pulse(pulse_intensity, pulse_duration)
                vbat1 = load.get_voltage()
            
            sleep(joulescope_config["final_wait"])
            joulescope.stop_capture()
            pulse_end_time = datetime.datetime.now()
            #Wait relaxation period
            sleep(RELAXATION_PERIOD - joulescope_config["final_wait"])

            with load: #Read voltage after relaxation
                load.pulse(0, 0)
                vbat2 = load.get_voltage()

            #Write battery voltage after relaxation into .csv file
            with open(output_filepath, 'a+', newline="", encoding='UTF-8') as file:
                writer = csv.writer(file)
                writer.writerow([pulse_start_time, count, vbat1, vbat2, pulse_end_time])
            
            if vbat2 <= CUTOFF_VOLTAGE:
                logger.error("Battery voltage after relaxation lower than CUTOFF_VOLTAGE")
                break 
            count += 1

    except KeyboardInterrupt:
        joulescope.stop_capture()
        logger.info("Test stopped using the keyboard interrupt")
    #Disconnected the battery from the joulescope
    joulescope.device.parameter_set('i_range', 'off')
    logger.info("Stopped discharge procedure")