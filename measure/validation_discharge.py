'''
This script needs to connect to variable load
Determine will it generate discharge or charge pulse
Based on that, from constrained interval, determine pulse current intensity, and duration
Apply the pulse
Measure the voltage
Save the data in csv file
Do that until some break condition
'''
import sys
sys.path.insert(0, '..')
import csv
import datetime

from time import sleep
from loguru import logger
from random import random, uniform
from variable_load.variable_load import VariableLoad


#CONFIGURATION:
variable_load_config = {
    "device_adress" : "GPIB0::24::INSTR"
}

output_filepath = ""
CUTOFF_VOLTAGE_1 = 2 #Before pulse
CUTOFF_VOLTAGE_2 = 1.5 #After pulse
discharge_amount = 1e-6 #in Ah
charge_amount = 0.5e-6 #in Ah 

#Discharging currents 50uA, 300uA
discharge_intensity_interval = [50e-6, 300e-6]
#Charging currents 10uA, 100uA
charge_intensity_interval = [10e-6, 100e-6]
SLEEP_TIME = 60

#Based on target charge_amount, calculates pulse duration in s
def calculate_duration(charge_amount, current):
    return (charge_amount/current)*3600


if __name__ == "__main__":
    logger.info("Opened validation_discharge.py script")

    load = VariableLoad(variable_load_config, compliance_voltage = 3.1)

    discharge_pulse = True #Is it discharge or charge pulse

    try:
        with load:
            while True:
                #One time charge, one time discharge
                if discharge_pulse: #Discharging pulse
                    #Odredi random vrijednost struje i dužine 
                    p_intensity = -1*uniform(discharge_intensity_interval[0],
                                            discharge_intensity_interval[1])
                    p_duration =  -1*calculate_duration(discharge_amount, 
                                                        p_intensity)
                else: #Charging pulse
                    #50% of the time, charge current is 0
                    if random() >= 0.5:
                        p_intensity = uniform(charge_intensity_interval[0],
                                            charge_intensity_interval[1])
                        p_duration = calculate_duration(charge_amount,
                                                        p_intensity)
                    else:
                        p_intensity = 0
                        p_duration = SLEEP_TIME


                load.set_current(0, 3.1)
                time1 = datetime.datetime.now()
                vbat1 = load.get_voltage()
                load.pulse(p_intensity, p_duration)
                time2 = datetime.datetime.now()
                vbat2 = load.get_voltage()
                
                sleep(1)

                with open(output_filepath, 'a+', newline="", encoding='UTF-8') as file:
                    writer = csv.writer(file)
                    writer.writerow([time1, vbat1, time2, vbat2])

                if vbat1 <= CUTOFF_VOLTAGE_1 and vbat2 <= CUTOFF_VOLTAGE_2:
                    logger.error("Battery voltage after relaxation lower than CUTOFF_VOLTAGE")
                    break
                
                discharge_pulse = not discharge_pulse

    except KeyboardInterrupt:
        logger.info("Test stopped using the keyboard interrupt")

    logger.info("Stopped discharge procedure")