'''
This serves as a interface for starting and stopping the .jls captures
'''
from joulescope import scan_require_one
from joulescope.data_recorder import DataRecorder

class Joulescope():
    def __init__(self, jls_config):
        self.device = scan_require_one(config='auto')
        self.recorder = None
        #Apply all of the other parameters to the device
        if "device_parameters" in jls_config:
            for parameter in jls_config["device_parameters"]:
                self.device.parameter_set(parameter, jls_config["device_parameters"][parameter])
        
    def __exit__(self, exc_type, exc_value, tb):
        self.device.stop()
        self.device.close()
        self.recorder.close()
        self.recorder = None

    def start_capture(self, filename):
        self.device.open()
        self.recorder = DataRecorder(filename, calibration=self.device.calibration)
        self.device.stream_process_register(self.recorder)
        self.device.start()

    def stop_capture(self):
        self.device.stream_process_unregister(self.recorder)
        self.device.stop()
        self.device.close()
        if self.recorder:
            self.recorder.close()
            self.recorder = None