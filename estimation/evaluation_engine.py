import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from datetime import datetime, timedelta
from estimator_engine import EstimatorEngine

#Get time
def get_time(string):
    return datetime.strptime(string, '%Y-%m-%d %H:%M:%S.%f')


v_init = 3
t_init = datetime.strptime("2022-03-17 15:01:00.743793", '%Y-%m-%d %H:%M:%S.%f') #Apsolutno vrijeme polaska eksperimenta
data_filepath = "../data/jls_captures_data.csv"
EFFECTIVE_CHARGE_AMOUNT = 8.333e-7

if __name__ == "__main__":
    engine = EstimatorEngine(v_init, t_init)

    actual, estimated, soc_from_vbat, modified  = [], [], [], []
    show_data = False

    df = pd.read_csv(data_filepath, header = None)
    modified = [np.nan]*len(df[0])
    for index, row in df.iterrows():    
        v1 = row[4] #Napon neposredno prije pulsa v2 #Trebalo bi 5, budimo iskreni, al aj
        v2 = row[8] #Napon neposredno nakog pulsa v2'

        Qeff = EFFECTIVE_CHARGE_AMOUNT #Charge spent on pulse
        actual_soc = 1 - (index)/len(df[0])
        current_time = get_time(row[2]) + timedelta(seconds = row[3])

        t_sleep =  current_time - engine.last_wu_time
        t_sleep = t_sleep.total_seconds()

        #Before the task
        engine.measure(v1,t_sleep)
        engine_result1 = str(engine)

        t_end = current_time + timedelta(seconds = row[6]) #Vrijeme završetka pulsa
        #After the task
        engine.update_charge(Qeff, v2, t_end)
        engine_result2 = str(engine)
        
        actual.append(actual_soc)
        estimated.append(engine.SoC)
        soc_from_vbat.append(engine.SoC_est)
        if engine.modified:
            modified[index] = engine.SoC
        if show_data:
            print("--------------------------------------------------------------------------")
            print(f"            V1 = {v1}   V2 = {v2}     sleep_t = {t_sleep}")
            print(f"1: {engine_result1}")
            print(f"2: {engine_result2}")
    #Open results.csv 

    x = np.linspace(0, 1, len(actual))
    plt.plot(x, actual, linewidth=2)
    plt.plot(x, estimated, linewidth=2)
    #plt.plot(x, soc_from_vbat)
    #plt.plot(x, modified)

    #plt.axvspan(1.25, 1.55, facecolor='#FF0000', alpha = 0.3)
    plt.legend(["Actual SoC", "Engine estiamted", "SoC from OCV bat"])
    plt.xlabel("Depth of Discharge [%]")
    plt.ylabel("State of Charge [%]")
    
    start_coord = None
    for i,value in enumerate(modified):
        if not np.isnan(value):
            if not start_coord: start_coord = x[i]
        elif start_coord:
            plt.axvspan(start_coord, x[i], facecolor='#FF0000', alpha = 0.3)
            start_coord = None    
    

    plt.show()