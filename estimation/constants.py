adc_scaling = float(3.3/4095) 

CHARGE_TRESHOLD = 0.05
CHARGE = 0.03
DISCHARGE_TRESHOLD = -0.05
DISCHAGE = -0.04

RELAXATION_TIME  = 30

#Podešavanje svih pinova i konfiguracijiskih stvari za estimator
vbat_pin = 'X22'
Qn = 3.219e-3 # Capacity for nominal discharge curve [mAh]
Qmax = 3.25e-3 #Absolute maximal battery capacity [mAh]
In = 15e-6 #Current value for nominal discharge curve[mA]

#Equivalent Circuit Model for battery
K = 0.008
tau1 = 0.75
tau2 = 300
# R1 = 1
# R2 = 1
# C1 = 1
# C2 = 1
# Rs = 1

#SoC(Voc_bat) obtained from nominal discharge curve
SOC_FROM_VBAT = [
        (2.09862069,	0),
        (2.1,	0.002122768),
        (2.211724138,	0.020963561),
        (2.363448276,	0.053416483),
        (2.437931034,	0.093396919),
        (2.495862069,	0.144666178),
        (2.559310345,	0.210139398),
        (2.622758621,	0.328804109),
        (2.719310345,	0.556806065),
        (2.788275862,	0.784710198),
        (2.832413793,	1)
]

#Vbat(SoC) obtained from nominal discharge curve, inverse of SoC(Voc_bat)
#(SoC, Vbat)
VBAT_FROM_SOC = [(val[1], val[0]) for val in SOC_FROM_VBAT]

#Peukert's coeffitiens pc(I)
PC_COEFF = [
    #(I, pc)
    (5e-6, 1.19),
    (15e-6, 1.19),
    (50e-6, 1.19),
    (100e-6, 1.19)
]