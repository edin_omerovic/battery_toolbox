import sys
import constants as config
import numpy as np

#Clipping function
def clip(x, min, max):
    if x > max: return max
    if x < min: return min
    return x
#Linear interpolation
def interpolate(x, array):
    if x < array[0][0]: return array[0][1]
    if x > array[-1][0]: return array[-1][1]
    for i, val in enumerate(array):
        if val[0] == x: return val[1] 
        elif val[0] > x:
            (x1, y1) = array[i - 1]
            (x2, y2) = array[i]
            y = (x*(y1 - y2) + x1*y2 - x2*y1)/(x1 - x2)
            return y
    return None


class EstimatorEngine():
    def __init__(self, v_init, time_init):
        self.Qtotal = 0 #Total charge outputed by battery
        self.Qnominal = config.Qn
        self.Qmax = config.Qmax
        self.Inominal = config.In
        self.vbat_pin = "bat"
        #self.vbat_pin = pyb.ADC(pyb.Pin(config.vbat_pin))
        self.Vbat = v_init
        #self.Vestimated = self.Vbat #Open circuit battery voltage estimated using SoC
        self.SoC = self.soc_from_ocv(self.Vbat)
        self.SoC_est = self.soc_from_ocv(self.Vbat)
        self.last_wu_time = time_init
    #Procedure done before applying load to battery
    def measure(self, v_bat1, tsleep):
        v_bat = v_bat1
        time_sleeping = tsleep
        #initial SoC estimation based on OCV 
        vbat_oc = self.estimate_vbat_oc(self.Vbat, v_bat, time_sleeping)
        self.SoC_est = self.soc_from_ocv(vbat_oc)
        self.modified = False
    
    def update_charge(self, Qeff, vbat_2, t_end):
        #Pay the bill
        self.modified = False
        self.Qtotal += Qeff 
        self.SoC = self.get_soc_from_q(self.Qtotal)
        
        self.SoC_est += -Qeff/self.Qnominal

        #CORRECTION
        err = self.SoC_est - self.SoC
        if(err > config.CHARGE_TRESHOLD or err < config.DISCHARGE_TRESHOLD):
            self.SoC += clip(err, config.DISCHAGE, config.CHARGE)
            Q_corrected = (1 - self.SoC) * self.Qnominal
            self.Qtotal += (Q_corrected - self.Qtotal)
            self.modified = True

        self.last_wu_time = t_end
        self.Vbat = vbat_2

    #Based on ECM of battery, estimate ideal open circuit voltage
    def estimate_vbat_oc(self, v0, vbat, t):
        vbat_oc = (1 - np.exp(-t/config.tau2))**(-1) * (vbat - v0*np.exp(-t/config.tau2) + config.K*(np.exp(-t/config.tau1) - np.exp(-t/config.tau2)))
        return vbat_oc + 0.1 #Hardcoded voltage jump to compensate for bad soc-ocv
    #Get SoC(V) obtained from nominal discharge curve
    def soc_from_ocv(self, Vbat_oc):
        return interpolate(Vbat_oc, config.SOC_FROM_VBAT)
    #Get Vbat(SoC) obtained from nominal discharge curve
    def get_vbat_from_soc(self, soc):
        return interpolate(soc, config.VBAT_FROM_SOC)

    def get_soc_from_q(self, Qtotal):
        soc = (self.Qmax - Qtotal)/self.Qnominal
        return clip(soc, 0, 1)
    #Effective current
    def Ieff(self, I):
        #Peukert's coeff
        pc = interpolate(I, config.PC_COEFF)
        return I**pc * self.Inominal**(1-pc)

    def __str__(self):
        if self.modified: s = "*"
        else: s = ""
        s += "SoC= {}, Qt= {}, Vbat = {}, SoCest_ocv = {}".format(self.SoC, self.Qtotal, self.Vbat, self.SoC_est)
        return s