#Ubacis filepath fajla sa raw konstantama

import argparse
import json
import os

from loguru import logger

def get_parser():
    p = argparse.ArgumentParser(
        description='Filepath .json config')
    p.add_argument('root_path',
                   help='Location of .json config file')
    return p

if __name__ == "__main__":
    logger.success("Opened experimet.py script")
    args = get_parser().parse_args()
    file_path = args.root_path

    #Otvori fajl, citaj liniju po liniju, napravi separaciju na bazi separatora
    #Upiši negdje u formatu kojiem ti to želiš
    with open(file_path) as f:
        line = f.readline()
        while line:
            data = line.split("\t")
            print(f"({data[0].strip()}, {data[1].strip()}),")
            line = f.readline()