'''
This script handles the communication with the pyboard RUNNING THE SOC ESTIMATOR ENGINE

'''

import os
from time import sleep
from serial import Serial

class Pyboard:
    def __init__(self, port, baud_rate):
        #Provjeriš da li postoji specificirani com port
        if not os.path.exists(port):
            #logger.error(f"Could not find .json config file on specified location")
            exit()
        self.serial = Serial(port, baud_rate)
    

    def read_line(self):
        while self.serial.in_waiting == 0: pass
        x = self.serial.readline()
        return x.decode('unicode_escape')
    
    def get_soc(self):
        self.serial.write(b'GET_SOC')
        return self.read_line().rstrip()

    def start_discharge(self, current, time):
        self.serial.write(b'DISCHARGE_START')
        resp = self.read_line().rstrip()
        if resp != "OK": return False
        
        sleep(0.05)

        command  = f"{current};{time}"
        self.serial.write(command.encode('UTF-8'))
        response_data = self.read_line().split(";")
        #print(response_data)
        #The last segment of response_data has a "\r\n"
        response_data[-1].replace("\r\n","")
        #Removing the whitepaces
        response_data = [data.strip() for data in response_data]
        #Checking if the respose is matching the expected response
        if (response_data[0] == "OK" and
            response_data[1] == str(current) and
            response_data[2] == str(time)):
            return True
        return False
        #Od ovog momenta znaš da je primio o struji i vremenu
        #I da je napravio mjerenje
        

    #Posalji signal Pyboardu, javi mu da je gotovo
    def finished_discharge(self):
        self.serial.write(b'DISCHARGE_DONE')
        #print(self.read_line())
        return True if self.read_line().rstrip() == "OK" else False