'''
This script will generate .csv file with important data for battery parameters estimation
Supply the folder od .jls files and corresponding .csv file 

Example:
Script writes the .csv row with:
[file, file_order, capture_start_time, t1, v1, v11, t2, v2, v22, duration]
../../experiment/measurements/measurements\1.jls,1,2022-03-18 15:01:38.743793,3.1199999999999997,3.074143,3.0638328,33.13,3.015647,3.0250812,39.045

file: absoulute filepath of the capture
file_order: # of capture
capture_start_time: absolute time of start
t1: relative time of initial voltage drop [s]
v1: voltage just before voltage drop
v11: voltage just after voltage drop
v2: voltage just before voltage jump
v22: voltage just after voltage jump
duration: duration of the whole capture [s]

specify the folder path of the folder of .jls captures: folder_path
specify the .csv file: csv_filepath
output_filename: the name .csv file for 
'''
import re
import csv
import glob
import numpy as np
import pandas as pd
from datetime import datetime
from dateutil.relativedelta import relativedelta
from joulescope.data_recorder import DataReader

folder_path = "../../experiment/measurements/measurements"
csv_filepath = "C:/Users/edin.omerovic/Desktop/Internship project/!enocean_projects/power_management_estimation/scripts/experiment/measurements/measurements/test.csv"
output_filename = "jls_captures_data.csv"

#HARDCODED TIME interval in seconds
intrest_range_data1 = [2.8, 3.3]
intrest_range_data2 = [32.5, 34]

def get_voltages(filepath):
    jls_reader = DataReader()
    jls_reader.open(filepath)
    duration = jls_reader.duration
    range1 = intrest_range_data1[1] - intrest_range_data1[0]
    range2 = intrest_range_data2[1] - intrest_range_data2[0]

    data1 = jls_reader.samples_get(intrest_range_data1[0], intrest_range_data1[1], units='seconds', fields=['current', 'voltage'])
    data2 = jls_reader.samples_get(intrest_range_data2[0], intrest_range_data2[1], units='seconds', fields=['current', 'voltage'])

    jls_reader.close()

    voltage_1 = data1['signals']['voltage']['value']
    current_1 = data1['signals']['current']['value']
    voltage_2 = data2['signals']['voltage']['value']
    current_2 = data2['signals']['current']['value']

    #voltage drop due to series resistance
    index_of_biggest_change = np.argmax(np.diff(np.diff(current_1)))
    #voltage just before the start pulse
    v1 = voltage_1[index_of_biggest_change]
    t1 = intrest_range_data1[0] + ((index_of_biggest_change + 1)/len(current_1))*range1
    index_of_biggest_change += 3
    #voltage just after the start pulse
    v11 = voltage_1[index_of_biggest_change] 
    
    index_of_biggest_change = np.argmin(np.diff(np.diff(current_2)))
    #voltage just before the end of pulse
    v2 = voltage_2[index_of_biggest_change]
    t2 = intrest_range_data2[0]+ ((index_of_biggest_change + 1)/len(current_2))*range2
    index_of_biggest_change += 3
    #voltage just after the end of pulse
    v22 = voltage_2[index_of_biggest_change]

    return v1, v11, v2, v22, t1, t2, duration

#s is a full path
def alphanum_key(s):
    filename = s.replace(folder_path + "\\", "")
    for c in re.split('\\.', filename):
        if c.isnumeric: return int(c)

#Read the specified .csv file and get the time of recordign
def get_timestamp(count):
    df = pd.read_csv(csv_filepath, header=None)
    #find the row with that count
    row = df.loc[df[1] == count]
    return row.iat[0,0]

if __name__ == "__main__":
    fileslist = glob.glob(folder_path + "/*.jls")    
    fileslist.sort(key=alphanum_key)

    for file in fileslist:
        file_order = alphanum_key(file)
        time_stamp = datetime.strptime(get_timestamp(file_order), '%Y-%m-%d %H:%M:%S.%f')
        v1, v11, v2, v22, t1, t2, duration = get_voltages(file)
        #capture_start_time = capture_end_time - capture_duration - pause_duration
        capture_start_time = time_stamp - relativedelta(seconds=(30 + duration)) #Harcoded
        #Ove informacije možeš povlačiti i iz novog .csv file
        
        print(f"count: {file_order} t: {capture_start_time}, t1:{t1},  V1: {v1}, V1':{v11}, t2:{t2}, V2:{v2}, V2':{v22}, duration:{duration}")
        with open(output_filename, "a+", newline="", encoding="utf-8") as f:
            writer = csv.writer(f)
            writer.writerow([file, file_order, capture_start_time, t1, v1, v11, t2, v2, v22, duration])

