'''
Simple periodic pulse generation

'''

from time import sleep
from variable_load import VariableLoad



if __name__ == "__main__":
    config = {"device_adress" : "GPIB0::24::INSTR"}
    sleep_time  = 5
    pulse_current = -15e-6
    pulse_time = 2

    while True:
        print(f"Started the {pulse_current} pulse for {pulse_time} seconds")
        with VariableLoad(config, compliance_voltage = 3.1) as load:
            load.pulse(pulse_current, pulse_time) 
        print("Finished the pulse")
        print(f"Going to sleep for {sleep_time} seconds")
        sleep(sleep_time)