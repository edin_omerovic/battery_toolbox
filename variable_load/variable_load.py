from time import sleep
from pymeasure.instruments.keithley import Keithley2400

class VariableLoad():
    def __init__(self, config, compliance_voltage = 3.1):
        self.device = Keithley2400(config["device_adress"])
        self.device.source_current = 0
        self.compliance_voltage = compliance_voltage
        self.running = False

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, tb):
        self.device.source_current = 0
        self.device.disable_source()
        self.device.shutdown()
        return None

    def pulse(self, discharge_current, time):
        self.device.apply_current()
        self.device.auto_range_source()
        self.device.compliance_voltage = self.compliance_voltage
        self.device.source_current = discharge_current
        #Presses the enable output
        self.device.enable_source()
        self.running = True
        sleep(time) #Time blocking
        self.device.source_current = 0
        #self.device.disable_source()
        self.running = False

    def set_current(self, discharge_current, compliance_voltage):
        self.device.apply_current()
        self.device.auto_range_source()
        self.device.compliance_voltage = self.compliance_voltage
        self.device.source_current = discharge_current
        #Presses the enable output
        self.device.enable_source()
        self.running = True if discharge_current != 0 else False
    
    def get_voltage(self):
        self.device.measure_voltage()
        return self.device.voltage